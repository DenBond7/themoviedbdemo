package com.denbond7.themoviedbdemo;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.denbond7.themoviedbdemo.retrofit.BaseResponse;
import com.denbond7.themoviedbdemo.retrofit.request.api.discover.movie.DiscoverMovieRequest;
import com.denbond7.themoviedbdemo.retrofit.response.api.discover.movie.DiscoverMovieResponse;
import com.denbond7.themoviedbdemo.retrofit.response.model.DiscoverMovieResult;
import com.denbond7.themoviedbdemo.ui.adpters.MovieListBaseAdapter;
import com.denbond7.themoviedbdemo.ui.loader.ApiServiceAsyncTaskLoader;
import com.denbond7.themoviedbdemo.ui.views.EndlessScrollListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<BaseResponse> {
    public static final String KEY_PAGE = "page";
    public static final int LOADER_ID = 0;
    public static final int COUNT_OF_MONTH_FOR_SEARCH = 2;
    private View emptyView;
    private ListView listView;
    private ProgressBar progressBar;
    private ProgressBar toolbarProgressBar;
    private int currentPage = 1;
    private int totalPageCount;
    private long currentDate, dateAfterTwoMonth;
    private MovieListBaseAdapter movieListBaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        currentDate = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, COUNT_OF_MONTH_FOR_SEARCH);
        dateAfterTwoMonth = calendar.getTimeInMillis();

        emptyView = findViewById(R.id.emptyView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        toolbarProgressBar = (ProgressBar) findViewById(R.id.toolbarProgressBar);
        listView = (ListView) findViewById(R.id.listView);

        movieListBaseAdapter = new MovieListBaseAdapter(this, new ArrayList<DiscoverMovieResult>());
        listView.setAdapter(movieListBaseAdapter);

        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                loadNextDataFromApi(page);
                return currentPage != totalPageCount;
            }
        });

        loadNextDataFromApi(currentPage);
    }

    @Override
    public Loader<BaseResponse> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_ID:
                int page = 1;
                if (args != null) {
                    page = args.getInt(KEY_PAGE);
                }

                if (currentPage != 1) {
                    toolbarProgressBar.setVisibility(View.VISIBLE);
                }
                return new ApiServiceAsyncTaskLoader(this, new DiscoverMovieRequest(currentDate, dateAfterTwoMonth,
                        page));

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<BaseResponse> loader, BaseResponse data) {
        switch (loader.getId()) {
            case LOADER_ID:
                toolbarProgressBar.setVisibility(View.GONE);
                if (data != null) {
                    if (data.getResponseModel() != null) {
                        DiscoverMovieResponse discoverMovieResponse = (DiscoverMovieResponse) data.getResponseModel();
                        totalPageCount = discoverMovieResponse.getTotalPages();
                        currentPage = discoverMovieResponse.getPage();
                        if (!TextUtils.isEmpty(discoverMovieResponse.getStatusMessage())) {
                            Snackbar.make(listView, getString(R.string.error_message,
                                    discoverMovieResponse.getStatusCode(), discoverMovieResponse.getStatusMessage()),
                                    Snackbar.LENGTH_LONG).show();
                        } else if (discoverMovieResponse.getDiscoverMovieResults() != null
                                && !discoverMovieResponse.getDiscoverMovieResults().isEmpty()) {
                            if (listView.getVisibility() != View.VISIBLE) {
                                listView.setVisibility(View.VISIBLE);
                            }
                            progressBar.setVisibility(View.GONE);
                            emptyView.setVisibility(View.GONE);
                            displayResults(discoverMovieResponse.getDiscoverMovieResults());
                        } else if (movieListBaseAdapter.getCount() == 0) {
                            progressBar.setVisibility(View.GONE);
                            listView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    } else if (data.getException() != null) {
                        Snackbar.make(listView, data.getException().getMessage(), Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(listView, R.string.unknown_error, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(listView, R.string.some_error_with_api_call, Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseResponse> loader) {

    }

    private void displayResults(List<DiscoverMovieResult> discoverMovieResults) {
        if (movieListBaseAdapter != null) {
            movieListBaseAdapter.appendResults(discoverMovieResults);
        }
    }

    private void loadNextDataFromApi(int page) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_PAGE, page);
        getSupportLoaderManager().restartLoader(LOADER_ID, bundle, this);
    }
}
