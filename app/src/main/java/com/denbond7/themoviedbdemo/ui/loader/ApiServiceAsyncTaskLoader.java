package com.denbond7.themoviedbdemo.ui.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.denbond7.themoviedbdemo.retrofit.ApiHelper;
import com.denbond7.themoviedbdemo.retrofit.ApiService;
import com.denbond7.themoviedbdemo.retrofit.BaseResponse;
import com.denbond7.themoviedbdemo.retrofit.request.BaseRequest;
import com.denbond7.themoviedbdemo.retrofit.request.api.discover.movie.DiscoverMovieRequest;
import com.denbond7.themoviedbdemo.retrofit.response.api.discover.movie.DiscoverMovieResponse;

public class ApiServiceAsyncTaskLoader extends AsyncTaskLoader<BaseResponse> {
    private ApiHelper apiHelper;
    private BaseRequest baseRequest;
    private ApiService apiService;

    public ApiServiceAsyncTaskLoader(Context context, BaseRequest baseRequest) {
        super(context);
        this.apiHelper = ApiHelper.getInstance(context);
        this.baseRequest = baseRequest;
        onContentChanged();
    }

    @Override
    public void onStartLoading() {
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    public BaseResponse loadInBackground() {
        if (apiHelper != null && apiHelper.getRetrofit() != null) {
            apiService = apiHelper.getRetrofit().create(ApiService.class);

            if (baseRequest != null && baseRequest.getApiName() != null) {
                switch (baseRequest.getApiName()) {
                    case GET_DISCOVER_MOVIE:
                        BaseResponse<DiscoverMovieResponse> discoverMovieResponse = new BaseResponse<>();
                        discoverMovieResponse.setApiName(baseRequest.getApiName());

                        DiscoverMovieRequest discoverMovieRequest = (DiscoverMovieRequest) baseRequest;

                        if (apiService != null) {
                            try {
                                discoverMovieResponse.setResponse(
                                        apiService.getDiscoverMovie(discoverMovieRequest.getQueryMap()).execute());
                            } catch (Exception e) {
                                e.printStackTrace();
                                discoverMovieResponse.setException(e);
                            }
                        }
                        return discoverMovieResponse;
                }
            }
        }

        return null;
    }

    @Override
    public void onStopLoading() {
        cancelLoad();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApiServiceAsyncTaskLoader)) return false;

        ApiServiceAsyncTaskLoader that = (ApiServiceAsyncTaskLoader) o;

        if (apiHelper != null ? !apiHelper.equals(that.apiHelper) : that
                .apiHelper != null)
            return false;
        if (baseRequest != null ? !baseRequest.equals(that.baseRequest) : that.baseRequest != null)
            return false;
        return !(apiService != null ? !apiService.equals(that.apiService) : that.apiService !=
                null);

    }

    @Override
    public int hashCode() {
        int result = apiHelper != null ? apiHelper.hashCode() : 0;
        result = 31 * result + (baseRequest != null ? baseRequest.hashCode() : 0);
        result = 31 * result + (apiService != null ? apiService.hashCode() : 0);
        return result;
    }
}
