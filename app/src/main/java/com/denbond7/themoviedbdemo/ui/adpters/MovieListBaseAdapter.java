package com.denbond7.themoviedbdemo.ui.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.denbond7.themoviedbdemo.R;
import com.denbond7.themoviedbdemo.retrofit.response.model.DiscoverMovieResult;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Bondarenko
 *         Date: 04.10.2017
 *         Time: 11:39
 *         E-mail: DenBond7@gmail.com
 */

public class MovieListBaseAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<DiscoverMovieResult> discoverMovieResults;

    public MovieListBaseAdapter(Context context,
                                List<DiscoverMovieResult> discoverMovieResults) {
        this.discoverMovieResults = discoverMovieResults;
        this.inflater = LayoutInflater.from(context);

        if (this.discoverMovieResults == null) {
            this.discoverMovieResults = new ArrayList<>();
        }
    }

    @Override
    public int getCount() {
        return discoverMovieResults.size();
    }

    @Override
    public DiscoverMovieResult getItem(int position) {
        return discoverMovieResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiscoverMovieResult discoverMovieResult = getItem(position);

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.discover_movie_item, parent, false);

            viewHolder.textViewPosition = convertView.findViewById(R.id.textViewPosition);
            viewHolder.textViewHeader = convertView.findViewById(R.id.textViewHeader);
            viewHolder.textViewTitle = convertView.findViewById(R.id.textViewTitle);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewPosition.setText(String.valueOf(position));
        viewHolder.textViewHeader.setText(discoverMovieResult.getTitle());
        viewHolder.textViewTitle.setText(discoverMovieResult.getReleaseDate());


        return convertView;
    }

    public void appendResults(List<DiscoverMovieResult> discoverMovieResults) {
        if (discoverMovieResults != null && !discoverMovieResults.isEmpty()) {
            this.discoverMovieResults.addAll(discoverMovieResults);
            notifyDataSetChanged();
        }
    }

    private class ViewHolder {
        TextView textViewHeader;
        TextView textViewTitle;
        TextView textViewPosition;
    }
}
