package com.denbond7.themoviedbdemo.retrofit.response.api.discover.movie;

import android.os.Parcel;

import com.denbond7.themoviedbdemo.retrofit.response.base.BaseApiResponse;
import com.denbond7.themoviedbdemo.retrofit.response.model.DiscoverMovieResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Denis Bondarenko
 *         Date: 04.10.2017
 *         Time: 10:42
 *         E-mail: DenBond7@gmail.com
 */

public class DiscoverMovieResponse extends BaseApiResponse {
    public static final Creator<DiscoverMovieResponse> CREATOR = new Creator<DiscoverMovieResponse>() {
        @Override
        public DiscoverMovieResponse createFromParcel(Parcel source) {
            return new DiscoverMovieResponse(source);
        }

        @Override
        public DiscoverMovieResponse[] newArray(int size) {
            return new DiscoverMovieResponse[size];
        }
    };
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total_results")
    @Expose
    private Integer totalResults;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("results")
    @Expose
    private List<DiscoverMovieResult> discoverMovieResults = null;

    public DiscoverMovieResponse() {
    }

    protected DiscoverMovieResponse(Parcel in) {
        super(in);
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalResults = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalPages = (Integer) in.readValue(Integer.class.getClassLoader());
        this.discoverMovieResults = in.createTypedArrayList(DiscoverMovieResult.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.page);
        dest.writeValue(this.totalResults);
        dest.writeValue(this.totalPages);
        dest.writeTypedList(this.discoverMovieResults);
    }

    @Override
    public String toString() {
        return "DiscoverMovieResponse{" +
                "page=" + page +
                ", totalResults=" + totalResults +
                ", totalPages=" + totalPages +
                ", discoverMovieResults=" + discoverMovieResults +
                "} " + super.toString();
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<DiscoverMovieResult> getDiscoverMovieResults() {
        return discoverMovieResults;
    }

    public void setDiscoverMovieResults(List<DiscoverMovieResult> discoverMovieResults) {
        this.discoverMovieResults = discoverMovieResults;
    }
}
