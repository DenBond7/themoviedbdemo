package com.denbond7.themoviedbdemo.retrofit.request;

import com.denbond7.themoviedbdemo.Constants;
import com.denbond7.themoviedbdemo.retrofit.ApiName;

import java.util.LinkedHashMap;

public abstract class BaseRequest {
    protected static final String PARAMETER_NAME_API_KEY = "api_key";
    protected static final String REQUEST_DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    private LinkedHashMap<String, String> queryMap;
    private ApiName apiName;


    public BaseRequest(ApiName apiName) {
        this.apiName = apiName;
        queryMap = new LinkedHashMap<>();
        queryMap.put(PARAMETER_NAME_API_KEY, Constants.API_KEY);
    }

    public LinkedHashMap<String, String> getQueryMap() {
        return queryMap;
    }

    public void setQueryMap(LinkedHashMap<String, String> queryMap) {
        this.queryMap = queryMap;
    }

    public ApiName getApiName() {
        return apiName;
    }
}
