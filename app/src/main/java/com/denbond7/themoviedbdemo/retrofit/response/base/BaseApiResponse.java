package com.denbond7.themoviedbdemo.retrofit.response.base;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseApiResponse implements ApiResponse {
    public static final Creator<BaseApiResponse> CREATOR = new Creator<BaseApiResponse>() {
        @Override
        public BaseApiResponse createFromParcel(Parcel source) {
            return new BaseApiResponse(source);
        }

        @Override
        public BaseApiResponse[] newArray(int size) {
            return new BaseApiResponse[size];
        }
    };
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @SerializedName("success")
    @Expose
    private boolean success;

    public BaseApiResponse() {
    }

    protected BaseApiResponse(Parcel in) {
        this.statusMessage = in.readString();
        this.statusCode = in.readInt();
        this.success = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.statusMessage);
        dest.writeInt(this.statusCode);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public boolean isSuccess() {
        return success;
    }
}
