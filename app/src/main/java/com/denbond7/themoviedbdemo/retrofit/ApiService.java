package com.denbond7.themoviedbdemo.retrofit;

import com.denbond7.themoviedbdemo.retrofit.response.api.discover.movie.DiscoverMovieResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiService {
    public static final String API_VERSION = "3";

    @GET(API_VERSION + "/discover/movie")
    Call<DiscoverMovieResponse> getDiscoverMovie(@QueryMap Map<String, String> options);
}
