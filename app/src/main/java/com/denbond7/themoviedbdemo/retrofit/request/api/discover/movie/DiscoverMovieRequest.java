package com.denbond7.themoviedbdemo.retrofit.request.api.discover.movie;

import com.denbond7.themoviedbdemo.retrofit.ApiName;
import com.denbond7.themoviedbdemo.retrofit.request.BaseRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Denis Bondarenko
 *         Date: 04.10.2017
 *         Time: 10:42
 *         E-mail: DenBond7@gmail.com
 */

public class DiscoverMovieRequest extends BaseRequest {
    private static final String PARAMETER_NAME_PRIMARY_RELEASE_DATE_GTE = "primary_release_date.gte";
    private static final String PARAMETER_NAME_PRIMARY_RELEASE_DATE_LTE = "primary_release_date.lte";
    private static final String PARAMETER_NAME_PAGE = "page";

    public DiscoverMovieRequest(long beginDateInMilliseconds, long endDateInMilliseconds, int page) {
        super(ApiName.GET_DISCOVER_MOVIE);

        if (getQueryMap() != null) {
            getQueryMap().put(PARAMETER_NAME_PRIMARY_RELEASE_DATE_GTE, generateFortmatedDate(beginDateInMilliseconds));
            getQueryMap().put(PARAMETER_NAME_PRIMARY_RELEASE_DATE_LTE, generateFortmatedDate(endDateInMilliseconds));
            getQueryMap().put(PARAMETER_NAME_PAGE, String.valueOf(page));
        }
    }

    private String generateFortmatedDate(long dateInMilliseconds) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(REQUEST_DATE_FORMAT_YYYY_MM_DD, Locale.US);
        return simpleDateFormat.format(new Date(dateInMilliseconds));
    }
}
